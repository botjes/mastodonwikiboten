# Hi!

This is a script that finds yesterday's most viewed English language Wikipedia page and toots about it on [botsin.space@YesterdaysWiki](https://botsin.space/web/@YesterdaysWiki). 

Feel free to copy the code and make a version for another language. If you do, please credit me and let me know so I can follow your bot!